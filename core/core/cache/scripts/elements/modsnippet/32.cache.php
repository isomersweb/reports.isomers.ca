<?php  return 'if (empty($fields)) return;

$fields = $modx->getOption(\'fields\', $scriptProperties, \'*\');
$table = $modx->getOption(\'table\', $scriptProperties, \'orders\');
$join = $modx->getOption(\'join\', $scriptProperties, \'\');
$cellTpl = $modx->getOption(\'cellTpl\', $scriptProperties, \'report_cell_tpl\');
$cellLinkTpl = $modx->getOption(\'cellLinkTpl\', $scriptProperties, \'report_cell_link_tpl\');
$rowTpl = $modx->getOption(\'rowTpl\', $scriptProperties, \'report_row_tpl\');
$outerTpl = $modx->getOption(\'outerTpl\', $scriptProperties, \'report_outer_tpl\');

$limit = $modx->getOption(\'limit\',$scriptProperties, 10);
$offset = $modx->getOption(\'offset\',$scriptProperties, 0);
$addToOffset = $modx->getOption(\'addToOffset\',$scriptProperties, 0);
$offset = $offset + $addToOffset;

$sql_where = "";
if(!empty($_GET)) {
    $where = array();
    foreach ($_GET as $key => $val) {
        if ($key == \'q\' || $key == \'page\' || $key == \'hash\' || $key == \'pageId\' || empty($val)) continue;
        $where_key = "";
        if(strpos($fields, "$table.$key") === false) {
            $where_key = "`$key`";
        }
        else{
            $where_key = "$table.`$key`";
        } 
        if ($key == \'Email\' || $key == \'ProductName\' || $key == \'Type\' || $key == \'ODName\' || $key == \'FirstName\' || $key == \'LastName\') {
            $where[] = " $where_key LIKE \'%$val%\' ";    
        }
        else {
            $where[] = " $where_key = \'$val\' ";       
        }
    }
    if (count($where) > 0) {
        $sql_where = " WHERE " . implode(" AND ", $where);
    }
}
$host = \'localhost\';
$username = \'iso_adm\';
$password = \'xK*3!mWWgR1N\';
$dbname = \'iso_oldb\';
$port = 3306;
$charset = \'utf8\';
 
$dsn = "mysql:host=$host;dbname=$dbname;port=$port;charset=$charset";
$xpdo = new xPDO($dsn, $username, $password);
 
// Test your connection
//echo $o = ($xpdo->connect()) ? \'Connected\' : \'Not Connected\';
 
$data = $theader = "";

$sql = "SELECT $fields FROM $table $join $sql_where";
$totalResult = $xpdo->query($sql); 
$total = $totalResult->rowCount();
$total = $total - $addToOffset;
$total = $total < 0 ? 0 : $total;
echo \'qwerty\';

$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$modx->setPlaceholder($totalVar, $total);

$sql = "SELECT $fields FROM $table $join $sql_where LIMIT $offset, $limit";

//echo $sql;
$results = $xpdo->query($sql); 

$fields = str_replace("$table.", "", $fields);
$fields = explode(",", $fields);
foreach ($fields as $field) {
   $theader .= $modx->getChunk($cellTpl, array(\'cell\' => $field));
}

while ($result = $results->fetch(PDO::FETCH_ASSOC)) {
    $cells = "";
    foreach ($fields as $field) {
        if ($field == \'OID\' || $field == \'ProductID\') {
            $cells .= $modx->getChunk($cellLinkTpl, array(\'cell\' => $result[$field], "key" => $field));

        }
        else {
            $cells .= $modx->getChunk($cellTpl, array(\'cell\' => $result[$field]));
        }
    }
    $data .= $modx->getChunk($rowTpl,  array(\'row\' => $cells));
}
if (!empty($outerTpl)) {
    return $modx->getChunk($outerTpl, array(
        \'header\' => $theader,
        \'output\' => $data
    ));    
}
else {
    return $data;
}
return;
';